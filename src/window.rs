use std::path::Path;

use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib};

use crate::application::ExampleApplication;

mod imp {
    use super::*;

    #[derive(Default, Debug, gtk::CompositeTemplate)]
    #[template(resource = "/de/schmidhuberj/PictureInListIssue/ui/window.ui")]
    pub struct ExampleApplicationWindow {
        #[template_child]
        pub list_view: TemplateChild<gtk::ListView>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ExampleApplicationWindow {
        const NAME: &'static str = "ExampleApplicationWindow";
        type Type = super::ExampleApplicationWindow;
        type ParentType = gtk::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ExampleApplicationWindow {
        fn constructed(&self) {
            self.parent_constructed();
            self.obj().setup_list_view();
        }
    }

    impl WidgetImpl for ExampleApplicationWindow {}
    impl WindowImpl for ExampleApplicationWindow {}
    impl ApplicationWindowImpl for ExampleApplicationWindow {}
}

glib::wrapper! {
    pub struct ExampleApplicationWindow(ObjectSubclass<imp::ExampleApplicationWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl ExampleApplicationWindow {
    pub fn new(app: &ExampleApplication) -> Self {
        glib::Object::builder().property("application", app).build()
    }

    fn setup_list_view(&self) {
        // IMPORTANT: Sets up the listview to reproduce a bug.
        let imp = self.imp();

        let list_view = &imp.list_view;

        // Unimportant that this is a StringList, just 5 items.
        let model: gtk::StringList = (0..=5)
            .into_iter()
            .map(|number| number.to_string())
            .collect();

        let selection_model = gtk::NoSelection::new(Some(model));
        list_view.set_model(Some(&selection_model));

        let factory = gtk::SignalListItemFactory::new();
        factory.connect_setup(move |_, list_item| {
            // Insert a Picture for each item
            let texture = gtk::gdk::Texture::from_file(&gio::File::for_path(Path::new(
                "./data/icons/de.schmidhuberj.PictureInListIssue.svg",
            )))
            .expect("Icon to exist");
            let picture = gtk::Picture::for_paintable(&texture);
            picture.set_width_request(100);
            picture.set_height_request(100);
            // IMPORTANT: If uncommented, the pictures take the full space they require.
            // picture.set_can_shrink(false);
            let list_item = list_item
                .downcast_ref::<gtk::ListItem>()
                .expect("Needs to be ListItem");
            list_item.set_child(Some(&picture));
        });

        list_view.set_factory(Some(&factory));
    }
}
