# Pictures in ListView Issue

This project shows an issue with Pictures in ListViews. When running, you should see a window with a ListView containing 5 pictures. The issue in this case is that there is blank space left after the ListView.

When setting `can-shrink` of the `Picture`s to `false`, you will notice that all the pictures nicely fill up the blank space.

![Screenshot](https://gitlab.com/Schmiddiii/gtk-picture-in-list-issue/-/raw/master/data/screenshots/screenshot.png)

Note that there is also a (I think) related issue where `Picture`s can lead to a `ScrolledWindow` to not be able to scroll all the way up/down and cutting of the `ListView`.

## Compilation and Running

Compile and run with meson:

```
meson setup build -Dprofile=build
meson compile -C build
./build/src/picture-in-list-issue
```

## Important Parts

Note that this project is written in Rust, as I don't really know C. Nevertheless, I am pretty sure that this is also reproducible in C. This section highlights the main interesting parts of the issue (as the repository itself is pretty big due to using [gtk-rust-template](https://gitlab.gnome.org/World/Rust/gtk-rust-template)).

- Definition of the ListView: <https://gitlab.com/Schmiddiii/gtk-picture-in-list-issue/-/blob/master/data/resources/ui/window.ui#L10>
- Initializing the ListView: <https://gitlab.com/Schmiddiii/gtk-picture-in-list-issue/-/blob/master/src/window.rs#L57>
